package a;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import a.UserAgentWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

/**
 * Unit test for simple App.
 */
public class UserAgentTest 
{
    @Test
    public void shouldAnswerWithTrue() throws IOException, FileNotFoundException
    {   
        String inputTestFileName = "common_user_agent.test";
        try (BufferedReader testReader = new BufferedReader(new FileReader(inputTestFileName))) {
            String[] test_entry = testReader.readLine().split(", ");
            String userAgentStr = test_entry[0];
            int osTypeTrue = Integer.parseInt(test_entry[1]);
            UserAgentWritable userAgent = new UserAgentWritable(userAgentStr);
            IntWritable osType = userAgent.getOsType();
            assertTrue(String.format("expected os type %d, but got %d for %s", 
                            osTypeTrue, osType.get(), userAgentStr),
                        osType.get() == osTypeTrue);

        }             
    }
}
