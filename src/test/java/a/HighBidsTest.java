package a;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Test;

import a.HighBids.HighBidReducer;
import a.HighBids.LogEntryMapper;

/**
 * Unit test for simple App.
 */
public class HighBidsTest {

    @Test
    public void mapperConstructsLogEntry() throws IOException {
        String bidId = "bidId";
        int city = 0;
        String bidPriceStr = "277";
        String userAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)";
        String logLineWithBid = String.join("\t", bidId, "stamp", "logtype", "ipinYouId", userAgent, "ip", "region",
                String.valueOf(city), "adEchange", "domain", "url", "anonymousUrl", "adSlot", "adWidth", "adHeight",
                "adVisibility", "adFormat", "adFloorPrice", "creativeId", bidPriceStr, bidPriceStr, "landingPageUrl",
                "advertiserId", "userProfileIds");
        LogEntryWritable logEntry = new LogEntryWritable(logLineWithBid);

        new MapDriver<Object, Text, IntWritable, LogEntryWritable>()
        .withMapper(new LogEntryMapper())
        .withInput(new IntWritable(0), new Text(logLineWithBid))
        .withOutput(new IntWritable(0), logEntry)
        .runTest();

    }

    @Test
    public void reducerCountsOneEquallyHighBid() throws Exception {

        int highBidCountExpected = 1;

        int highBidCriteria = HighBids.HighBidReducer.getHighBid();

        String bidId = "bidId";
        int city = 4;
        URI cityDictHDFSUri = new URI ("/home/dima_toichkin/highBids/input/city.en.txt");
        String cityDictFilename = "/home/dima_toichkin/highBids/input/city.en.txt";
        String bidPriceStr = String.valueOf(highBidCriteria);
        String logLineWithBid = String.join("\t", bidId, "stamp", "logtype", "ipinYouId", "user-agent", "ip", "region",
                String.valueOf(city), "adEchange", "domain", "url", "anonymousUrl", "adSlot", "adWidth", "adHeight",
                "adVisibility", "adFormat", "adFloorPrice", "creativeId", "0", bidPriceStr, "landingPageUrl",
                "advertiserId", "userProfileIds");
        LogEntryWritable logEntry = new LogEntryWritable(logLineWithBid);

        ArrayList<LogEntryWritable> entriesList = new ArrayList<LogEntryWritable>(1);
        entriesList.add(logEntry);

        ReduceDriver<IntWritable, LogEntryWritable, Text, IntWritable> driver = new ReduceDriver<IntWritable, LogEntryWritable, Text, IntWritable>();
        driver.withReducer(new HighBidReducer());
        driver.addCacheFile(cityDictHDFSUri);
        
        CityDictionary cityDictionary = new CityDictionary(cityDictFilename);
        driver.withInput(new IntWritable(city), entriesList);

        String cityName = cityDictionary.get(city);
        
        driver.withOutput(new Text(cityName), new IntWritable(highBidCountExpected));
        driver.runTest();

    }

    /*
     * @Test public void reducerDoesntCountLowBids() throws IOException {
     * 
     * String reportTitle = HighBids.HighBidReducer.getReportTitle(); int
     * highBidCriteria = HighBids.HighBidReducer.getHighBid(); int
     * highBidCountExpected = 0;
     * 
     * int entriesLen = 100; Random randGenerator = new Random(); String bidId =
     * "bidId"; String city = "Moscow"; ArrayList<LogEntryWritable> entriesList =
     * new ArrayList<LogEntryWritable>(1); for(int i=0; i<=entriesLen; i++) { int
     * bidValue = randGenerator.nextInt(highBidCriteria);
     * 
     * String bidPriceStr = String.valueOf(bidValue); String logLineWithBid =
     * String.join("\t",bidId, "stamp", "logtype", "ipinYouId", "user-agent", "ip",
     * "region", city, "adEchange", "domain", "url", "anonymousUrl", "adSlot",
     * "adWidth", "adHeight", "adVisibility", "adFormat", "adFloorPrice",
     * "creativeId", "0", bidPriceStr, "landingPageUrl", "advertiserId",
     * "userProfileIds"); LogEntryWritable logEntry = new
     * LogEntryWritable(logLineWithBid);
     * 
     * entriesList.add(logEntry); }
     * 
     * 
     * 
     * new ReduceDriver<Text, LogEntryWritable, Text, IntWritable>()
     * .withReducer(new HighBidReducer()) .withInput(new Text(city), entriesList)
     * .withOutput(new Text(reportTitle), new IntWritable(highBidCountExpected))
     * .runTest();
     * 
     * }
     */
}
