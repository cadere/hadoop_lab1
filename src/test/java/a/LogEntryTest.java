package a;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class LogEntryTest 
{
    /**
     * Rigorous Test :-)
     */
    private final int linesCount = 5;
    @Test
    public void shouldAnswerWithTrue() throws IOException, FileNotFoundException
    {   
        String bidIdTrue = "bidId";
        int cityTrue = 1;
        int bidPriceTrue = 48;
        String bidPriceStrTrue = String.valueOf(bidPriceTrue);
        String logLineWithBid = String.join("\t",bidIdTrue,
        "stamp", "logtype", "ipinYouId", "user-agent", "ip", "region", String.valueOf(cityTrue), "adEchange", "domain",
         "url", "anonymousUrl", "adSlot", "adWidth", "adHeight", "adVisibility", "adFormat", "adFloorPrice",
            "creativeId", "", bidPriceStrTrue, "landingPageUrl", "advertiserId", "userProfileIds");


        LogEntryWritable logEntry = new LogEntryWritable(logLineWithBid);
        Text bidId = logEntry.getBidId();
        IntWritable bidPrice = logEntry.getBidPrice();
        IntWritable bidCity = logEntry.getBidCity();
        System.out.println(bidCity);
        System.out.println(bidId.toString().equals(bidIdTrue));
        System.out.println(bidPrice.get() == bidPriceTrue);
        assertTrue( bidId.toString().equals(bidIdTrue));
        assertTrue( bidPrice.get() == bidPriceTrue);
        assertTrue( bidCity.get() == cityTrue);
            
        
        
    }
    public void default_constructor() throws IOException, FileNotFoundException
    {   
        LogEntryWritable logEntry = new LogEntryWritable();
        assertTrue( true );
        
    }
}
