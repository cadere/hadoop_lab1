package a;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;


public class HighBids {
    

  public static class LogEntryMapper
    extends Mapper<Object, Text, IntWritable, LogEntryWritable>{
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      String fieldsStr = value.toString();
      LogEntryWritable logEntry = new LogEntryWritable(fieldsStr);
      IntWritable cityId = logEntry.getBidCity();
      
      context.write(cityId, logEntry);
    }
  }

  public static class HighBidReducer
       extends Reducer<IntWritable,LogEntryWritable,Text,IntWritable> {
    private static int highBidValue = 250;
    private CityDictionary cityDictionary;
    private HashMap<Integer, Integer> highBidCounter;
    protected void setup(Context context) throws IOException {
      cityDictionary = new CityDictionary(context);
      highBidCounter = new HashMap<>();
      
    }
    
    public void reduce(IntWritable cityId, Iterable<LogEntryWritable> logEntries,
                       Context context
                       ) throws IOException, InterruptedException {
      for (LogEntryWritable logEntry : logEntries) {
        IntWritable bidPrice = logEntry.getBidPrice();
        

        if (bidPrice.get() >= highBidValue) {
          
          if (highBidCounter.containsKey(cityId.get())) {
            int oldCount = highBidCounter.get(cityId.get());
            int newCount = oldCount + 1;
            highBidCounter.remove(cityId.get());
            highBidCounter.put(cityId.get(),newCount);
          }
          else 
            highBidCounter.put(cityId.get(), 1);
        }
      }
    }
    public static int getHighBid() {
      return highBidValue;
    }

    public void cleanup(Context context)
     throws IOException, InterruptedException {
      
       for (int cityId : cityDictionary.keySet()) {
          String city = cityDictionary.get(cityId);
          if (highBidCounter.containsKey(cityId)) {
            int bidCount = highBidCounter.get(cityId);
            context.write(new Text(city), new IntWritable(bidCount));
          }
          //else
          //context.write(new Text(city), new IntWritable(0));
        }
        
    }
    public String getCityById(int cityId) {
      return cityDictionary.get(cityId);
    }
}

   public static class OSTypePartitioner extends
   Partitioner < IntWritable, LogEntryWritable >
   {
      @Override
      public int getPartition(IntWritable cityId, LogEntryWritable logEntry, int numReduceTasks)
      {
        IntWritable osType = logEntry.getUserAgent();
         return osType.get() % numReduceTasks;
      }
   }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "Average len");
    job.setJarByClass(HighBids.class);
    job.setMapperClass(LogEntryMapper.class);
    

    job.addCacheFile(new URI ("/user/root/city.en.txt"));
    //job.addCacheFile(new URI ("input/city.en.txt"));
    
    job.setMapOutputKeyClass(IntWritable.class);
    job.setMapOutputValueClass(LogEntryWritable.class);
    //job.setCombinerClass(HighBidReducer.class);
    job.setPartitionerClass(OSTypePartitioner.class);
    job.setReducerClass(HighBidReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    job.setOutputFormatClass(SequenceFileOutputFormat.class);
    job.setNumReduceTasks(2);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);
    SequenceFileOutputFormat.setCompressOutput(job, true);


    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}