package a;
import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import java.io.DataInput;
import java.io.DataOutput;

import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class UserAgentWritable implements WritableComparable<UserAgentWritable>
{

    private Pattern[] osPatterns = {
        Pattern.compile("windows",Pattern.CASE_INSENSITIVE),
        Pattern.compile("(?!android)(linux)",Pattern.CASE_INSENSITIVE),
        Pattern.compile("(?=mac.{0,5}os.{0,5}x)(?=ipad)",Pattern.CASE_INSENSITIVE),
        Pattern.compile("mac.{0,5}os.{0,5}x",Pattern.CASE_INSENSITIVE)
    };
    private IntWritable operationSystemType;
    
    public UserAgentWritable(String userAgentStr) {
        operationSystemType = new IntWritable(matchOsType(userAgentStr));
    }
    public UserAgentWritable() {
        operationSystemType = new IntWritable();
    }
    private int matchOsType(String userAgentStr) {
        int patternNum = 0;
        for (Pattern patt : osPatterns) {
            Matcher matcher = patt.matcher(userAgentStr);
            if (matcher.find()) {
                break;
            }
            patternNum++;
        }
        return patternNum;
    }

    public int compareTo(UserAgentWritable o) 
    {
        return (operationSystemType.compareTo(o.getOsType()));
    }
 
   @Override
   public boolean equals(Object o) 
   {
     if (o instanceof UserAgentWritable) 
     {
       UserAgentWritable other = (UserAgentWritable) o;
       return operationSystemType.equals(other.getOsType());
     }
     return false;
   }
    public void readFields(DataInput in) throws IOException {
        operationSystemType.readFields(in);
    }
    public void write(DataOutput out) throws IOException {
        operationSystemType.write(out);
    }
    public IntWritable getOsType() {
        return operationSystemType;
    }

    public int hashCode() {
        return operationSystemType.hashCode();
    }
}