package a;
import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import java.io.DataInput;
import java.io.DataOutput;
import a.UserAgentWritable;

public class LogEntryWritable implements WritableComparable<LogEntryWritable>
{
    private Text bidId;
    private IntWritable bidPrice;
    private IntWritable cityId;
    private UserAgentWritable userAgent;
    private final int bidIdIndex = 0;
    private final int bidCityIndex = 7;
    private final int bidPriceIndex = 20;
    private final int userAgentIndex = 5;
    private String delimiter = "\u0009";
    public LogEntryWritable(String log_line) {
        String[] fields = log_line.split(delimiter);

        this.bidId = new Text(fields[bidIdIndex]);

        this.cityId = new IntWritable(Integer.parseInt(fields[bidCityIndex]));

        int bidPrice_ = Integer.parseInt(fields[bidPriceIndex]);
        this.bidPrice = new IntWritable(bidPrice_);

        this.userAgent = new UserAgentWritable(fields[userAgentIndex]);
    }
    public LogEntryWritable() {
        bidId = new Text();
        bidPrice = new IntWritable();
        userAgent = new UserAgentWritable();
    }
    
    public int compareTo(LogEntryWritable o) 
    {

        return (bidPrice.compareTo(o.bidPrice));
    }
 
   @Override
   public boolean equals(Object o) 
   {
     if (o instanceof LogEntryWritable) 
     {
       LogEntryWritable other = (LogEntryWritable) o;
       return bidId.equals(other.bidId) && userAgent.equals(other.userAgent);
     }
     return false;
   }
    public void readFields(DataInput in) throws IOException {
        bidId.readFields(in);
        bidPrice.readFields(in);
        userAgent.readFields(in);
    }
    public void write(DataOutput out) throws IOException {
        bidId.write(out);
        bidPrice.write(out);
        userAgent.write(out);
    }
    public Text getBidId() {
        return bidId;
    }
    public IntWritable getBidCity() {
        return cityId;
    }
    public IntWritable getBidPrice() {
        return bidPrice;
    }
    public IntWritable getUserAgent() {
        return userAgent.getOsType();
    }
    public int hashCode() {
        return bidId.hashCode()*bidPrice.hashCode()*userAgent.hashCode();
    }
}