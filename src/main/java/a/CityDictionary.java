package a;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class CityDictionary extends HashMap<Integer,String>
{
    private static final long serialVersionUID = 1L;
    
    private String delimeter = "\t";
    public CityDictionary(Context context) throws IOException {
        URI[] cacheFiles = context.getCacheFiles();
        if (cacheFiles != null && cacheFiles.length > 0)
        {   Path path = new Path(cacheFiles[0].toString());
            FileSystem fs = path.getFileSystem(context.getConfiguration()); // context of mapper or reducer
            FSDataInputStream fdsis = fs.open(path);
            
            try (BufferedReader dictReader = 
                new BufferedReader(new InputStreamReader(fdsis)))
            {
                fillDict(dictReader);
            }
        }
    }
    public CityDictionary(String filename) throws IOException {
        try (BufferedReader dictReader = 
                new BufferedReader(new FileReader(filename)))
        {
            fillDict(dictReader);
        }
    }
    private void fillDict(BufferedReader dictReader) throws IOException {
        String nextLine;
        super.clear();
        
        while ((nextLine = dictReader.readLine()) != null) {
            String[] dictEntry = nextLine.split(delimeter);
            int cityCode = Integer.parseInt(dictEntry[0]);
            String city = dictEntry[1];
            super.put(cityCode, city);
        }
    }
}